
package main

import (
	_ "github.com/mattn/go-sqlite3"
	"database/sql"
	"time"
	"sync"
	"errors"
)

type Image struct {
	Id int
	Nick string
	Url string
	Posted string
	Displayed string
}

var dbMutex sync.Mutex

const Day = time.Hour * 24

func OpenDatabase() error {
	db, err := sql.Open("sqlite3", Conf.DBFile)
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS image(id INTEGER PRIMARY KEY, nick INTEGER, url STRING, postdate STRING, dispdate STRING)")
	if err != nil {
		return err
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS nickname(id INTEGER PRIMARY KEY, nick STRING)")
	if err != nil {
		return err
	}
	return nil
}

func getNickId(db *sql.DB, nick string) (int, error) {
	var nickid int

	err := db.QueryRow("SELECT id FROM nickname WHERE nick = ?", nick).Scan(&nickid)
	if err != nil {
		if err == sql.ErrNoRows {
			result, err := db.Exec("INSERT INTO nickname(nick) VALUES (?)", nick)
			if err != nil {
				return 0, err
			}
			last, err := result.LastInsertId()
			if err != nil {
				return 0, err
			}
			nickid = int(last)
		} else {
			return 0, err
		}
	}

	return nickid, nil
}

func AddImage(nick, url string) error {
	dbMutex.Lock()
	defer dbMutex.Unlock()

	db, err := sql.Open("sqlite3", Conf.DBFile)
	if err != nil {
		return err
	}
	defer db.Close()

	var exists int
	err = db.QueryRow("SELECT count(id) FROM image WHERE url = ?", url).Scan(&exists)
	if err != nil {
		return err
	}

	if exists > 0 {
		return errors.New("image already added")
	}

	nickid, err := getNickId(db, nick)
	if err != nil {
		return err
	}

	_, err = db.Exec("INSERT INTO image(nick, url, postdate) VALUES (?, ?, ?)", nickid, url, time.Now().UTC().Format("2006-01-02 15:04:05"))
	if err != nil {
		return err
	}
	return nil
}

func DeleteImage(nick, date string) error {
	var id int
	var displayed int

	dbMutex.Lock()
	defer dbMutex.Unlock()

	db, err := sql.Open("sqlite3", Conf.DBFile)
	if err != nil {
		return err
	}
	defer db.Close()

	nickid, err := getNickId(db, nick)
	if err != nil {
		return err
	}

	err = db.QueryRow("SELECT id, CASE WHEN dispdate <= date('now') THEN 1 ELSE 0 END FROM image WHERE nick = ? AND postdate = ?", nickid, date).Scan(&id, &displayed)
	if err != nil {
		if err == sql.ErrNoRows {
			return errors.New("no uploads at this date")
		} else {
			return err
		}
	}

	if displayed != 0 {
		return errors.New("image already displayed")
	}

	_, err = db.Exec("DELETE FROM image WHERE id = ?", id)
	if err != nil {
		return err
	}
	return nil
}

func ListImages(id int, nick string, postBefore, postAfter, dispBefore, dispAfter time.Time, page int) ([]Image, error) {
	var query []byte
	var param []interface{}
	var current Image
	var result []Image

	today := asDate(time.Now().UTC())

	dbMutex.Lock()
	defer dbMutex.Unlock()

	db, err := sql.Open("sqlite3", Conf.DBFile)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	query = []byte("SELECT image.id, nickname.nick, url, postdate, ifnull(dispdate, '-') FROM image LEFT JOIN nickname ON image.nick = nickname.id")
	if id != 0 || nick != "" || !postBefore.IsZero() || !postAfter.IsZero() || !dispBefore.IsZero() || !dispAfter.IsZero() {
		query = append(query, []byte(" WHERE ")...)
		if id != 0 {
			query = append(query, []byte("image.id = ? AND ")...)
			param = append(param, id)
		}
		if nick != "" {
			query = append(query, []byte("nickname.nick = ? AND ")...)
			param = append(param, nick)
		}
		if !postAfter.IsZero() {
			query = append(query, []byte("date(postdate) >= ? AND ")...)
			param = append(param, postAfter.Format("2006-01-02"))
		}
		if !postBefore.IsZero() {
			query = append(query, []byte("date(postdate) <= ? AND ")...)
			param = append(param, postBefore.Format("2006-01-02"))
		}
		if !dispAfter.IsZero() && dispAfter.Unix() <= today.Unix() {
			query = append(query, []byte("date(dispdate) >= ? AND ")...)
			param = append(param, dispAfter.Format("2006-01-02"))
		}
		if !dispBefore.IsZero() && dispBefore.Unix() <= today.Unix() {
			query = append(query, []byte("date(dispdate) <= ? AND ")...)
			param = append(param, dispBefore.Format("2006-01-02"))
		}
		query = query[:len(query)-4]

	}

	if page >= 0 {
		query = append(query, []byte("ORDER BY image.id DESC LIMIT 5 OFFSET ?")...)
		param = append(param, page * 5)
	}

	rows, err := db.Query(string(query), param...)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(&current.Id, &current.Nick, &current.Url, &current.Posted, &current.Displayed)
		if err != nil {
			return nil, err
		}
		result = append(result, current)
	}

	return result, rows.Err()
}

func Display(date time.Time) (string, error) {
	var id int
	var url string

	dbMutex.Lock()
	defer dbMutex.Unlock()

	db, err := sql.Open("sqlite3", Conf.DBFile)
	if err != nil {
		return "", err
	}
	defer db.Close()

	err = db.QueryRow("SELECT id, url FROM image WHERE dispdate = ?", date.Format("2006-01-02")).Scan(&id, &url)
	if err != nil {
		if err == sql.ErrNoRows {
		Retry:
			err = db.QueryRow("SELECT id, url FROM image WHERE dispdate IS NULL").Scan(&id, &url)
			if err != nil {
				if err == sql.ErrNoRows {
					return "", nil
				}
				return "", err
			}

			err = verifyImage(url)
			if err != nil {
				_, err = db.Exec("DELETE FROM image WHERE id = ?", id)
				if err != nil {
					return "", err
				}
				goto Retry
			}

			date = asDate(date)
			_, err = db.Exec("UPDATE image SET dispdate = ? WHERE id = ?", date.Format("2006-01-02"), id)
			if err != nil {
				return "", err
			}
		} else {
			return "", err
		}
	}

	return url, nil
}
