
package main

import (
	"net/http"
	"errors"
	"strings"
	"time"
)

func verifyImage(url string) error {
	iproto := strings.IndexByte(url, ':')
	if iproto == -1 {
		return errors.New("invalid url")
	}

	proto := url[:iproto]
	switch proto {
	case "http", "https":
		client := http.Client{
			Timeout: time.Second * 30,
		}
		res, err := client.Get(url)
		if err != nil {
			return err
		}
		defer res.Body.Close()

		if res.StatusCode != 200 {
			return errors.New("got non-200 response from url")
		}
		contentType := strings.ToLower(res.Header.Get("Content-Type"))
		if contentType != "image/png" && contentType != "image/jpeg" && contentType != "image/gif" && contentType != "image/tiff" && contentType != "image/bmp" {
			return errors.New("got invalid content type from image")
		}
	default:
		return errors.New("unknown protocol")
	}

	return nil
}
