
package main

import (
	"github.com/Elemental-IRCd/irc"
	"strings"
	"fmt"
	"log"
	"flag"
	"time"
	"os"
	"strconv"
	"io/ioutil"
	"encoding/json"
)

type Config struct {
	Nick string
	User string
	Pass string

	Server string
	Channel string
	Topic string
	Label string
	NoLabel string

	DBFile string
}

var ConfFile = flag.String("config", "default.json", "configuration file")
var Conf Config

var lastTopic string

var Conn *irc.Connection
var ZeroTime = time.Date(1, time.January, 1, 0, 0, 0, 0, time.UTC)

func asDate(date time.Time) time.Time {
	return time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, date.Location())
}

func updateTopic(s string) {
	var newTopic string
	if s == "" {
		newTopic = fmt.Sprintf("%s %s", Conf.Topic, Conf.NoLabel)
	} else {
		newTopic = fmt.Sprintf("%s %s: %s", Conf.Topic, Conf.Label, s)
	}
	if newTopic != lastTopic {
		Conn.SendRawf("TOPIC %s :%s", Conf.Channel, newTopic)
		lastTopic = newTopic
	}
}

func handleCommand(e *irc.Event) {
	var date time.Time
	var err error
	if e.Arguments[0][0] == '#' {
		// Ignore messages from channels.
		return
	}

	log.Printf("<%s> %s", e.Nick, e.Arguments[1])
	split := strings.SplitN(e.Arguments[1], " ", 2)
	switch strings.ToUpper(split[0]) {
	case "GET":
		if len(split) == 1 {
			date = asDate(time.Now().UTC())
		} else {
			date, err = time.Parse("2006-01-02", split[1])
			if err != nil {
				Conn.Privmsgf(e.Nick, "error: %s", err.Error())
				return
			}
			if date.Unix() > asDate(time.Now().UTC()).Unix() {
				Conn.Privmsg(e.Nick, "error: cannot display future images")
				return
			}
		}

		images, err := ListImages(0, "", ZeroTime, ZeroTime, date, date, -1)
		if err != nil {
			Conn.Privmsgf(e.Nick, "error: %s", err.Error())
			return
		}
		if len(images) == 0 {
			Conn.Privmsgf(e.Nick, "error: no image available")
			return
		}
		Conn.Privmsgf(e.Nick, "%s: <%s> %s", images[0].Displayed, images[0].Nick, images[0].Url)
	case "POST":
		if len(split) == 1 {
			Conn.Privmsg(e.Nick, "error: no url specified")
			return
		}

		err = verifyImage(split[1])
		if err != nil {
			Conn.Privmsgf(e.Nick, "error: %s", err.Error())
			return
		}

		err = AddImage(e.Nick, split[1])
		if err != nil {
			Conn.Privmsgf(e.Nick, "error: %s", err.Error())
			return
		}
		date = asDate(time.Now().UTC())
		image, err := Display(date)
		if err != nil {
			Conn.Privmsgf(e.Nick, "error: %s", err.Error())
			return
		}
		updateTopic(image)
		Conn.Privmsg(e.Nick, "done")
	case "DELETE":
		if len(split) == 1 {
			Conn.Privmsg(e.Nick, "error: no date specified")
			return
		}
		err = DeleteImage(e.Nick, split[1])
		if err != nil {
			Conn.Privmsgf(e.Nick, "error: %s", err.Error())
			return
		}
		Conn.Privmsg(e.Nick, "done")
	case "POSTS":
		var i int
		var err error
		var images []Image

		if len(split) == 1 {
			images, err = ListImages(0, e.Nick, ZeroTime, ZeroTime, ZeroTime, ZeroTime, 0)
		} else {
			i, err = strconv.Atoi(split[1])
			if err != nil {
				Conn.Privmsgf(e.Nick, "error: %s", err.Error())
				return
			}
			if i < 1 {
				Conn.Privmsg(e.Nick, "error: invalid page")
				return
			}
			images, err = ListImages(0, e.Nick, ZeroTime, ZeroTime, ZeroTime, ZeroTime, i - 1)
		}
		if err != nil {
			Conn.Privmsgf(e.Nick, "error: %s", err.Error())
			return
		}
		if len(images) == 0 {
			Conn.Privmsgf(e.Nick, "error: no images available")
			return
		}
		for i := range images {
			if images[i].Displayed == "-" {
				Conn.Privmsgf(e.Nick, "%s: %s", images[i].Posted, images[i].Url)
			} else {
				Conn.Privmsgf(e.Nick, "%s [%s]: %s", images[i].Posted, images[i].Displayed, images[i].Url)
			}
		}
	case "HELP":
		if len(split) == 1 {
			Conn.Privmsg(e.Nick, "GET     fetches an image from a date")
			Conn.Privmsg(e.Nick, "POST    posts a new image")
			Conn.Privmsg(e.Nick, "DELETE  deletes a previously posted image")
			Conn.Privmsg(e.Nick, "POSTS   lists created posts")
			Conn.Privmsg(e.Nick, "HELP    displays this message")
		} else {
			switch strings.ToUpper(split[1]) {
			case "GET":
				Conn.Privmsg(e.Nick, "GET [date]")
				Conn.Privmsg(e.Nick, "fetches an image at a date, or the current date if none is specified")
				Conn.Privmsg(e.Nick, "date must be formatted in ISO-8601 format (YYYY-mm-DD)")

			case "POST":
				Conn.Privmsg(e.Nick, "POST <image>")
				Conn.Privmsg(e.Nick, "posts a new image and puts it in the display queue")
				Conn.Privmsg(e.Nick, "image must be a valid url, and must have an image format as the content type")

			case "DELETE":
				Conn.Privmsg(e.Nick, "DELETE <date>")
				Conn.Privmsg(e.Nick, "deletes a previously posted image and removes it from the display queue")
				Conn.Privmsg(e.Nick, "date must be formatted in ISO-8601 format (YYYY-mm-DD)")
				Conn.Privmsg(e.Nick, "images that has already been displayed cannot be deleted")

			case "POSTS":
				Conn.Privmsg(e.Nick, "POSTS [page]")
				Conn.Privmsg(e.Nick, "lists posts you have made, 5 at a time to avoid flooding")
				Conn.Privmsg(e.Nick, "page 1 will be shown if none is specified")

			case "HELP":
				Conn.Privmsg(e.Nick, "HELP [command]")
				Conn.Privmsg(e.Nick, "displays all commands or more details on a specific command")

			default:
				Conn.Privmsg(e.Nick, "error: unknown command")
			}
		}
	default:
		Conn.Privmsg(e.Nick, "error: unknown command")
	}
}

func loadConfig() error {
	flag.Parse()
	file, err := os.Open(*ConfFile)
	if err != nil {
		return err
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &Conf)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	err := loadConfig()
	if err != nil {
		log.Printf("configuration error: %s\n", err.Error())
		return;
	}
	Conn = irc.New(Conf.Nick, Conf.User)
	Conn.AddCallback("001", func (e *irc.Event) {
		Conn.Mode(Conf.Nick, "+R")
		if Conf.Pass != "" {
			Conn.Privmsgf("NickServ", "IDENTIFY %s", Conf.Pass)
		}
		Conn.Join(Conf.Channel)
	})
	Conn.AddCallback("331", func (e *irc.Event) {
		lastTopic = ""
	})
	Conn.AddCallback("332", func (e *irc.Event) {
		lastTopic = e.Arguments[2]
	})
	Conn.AddCallback("366", func (e *irc.Event) {
		go func () {
			var lastDate = ZeroTime
			for {
				date := asDate(time.Now().UTC())
				if date.Unix() != lastDate.Unix() {
					s, err := Display(date)
					if err == nil {
						updateTopic(s)
						lastDate = date
					} else {
						log.Printf("display error: %s", err.Error())
					}
				}
				time.Sleep(time.Minute * 5)
			}
		}()
	})
	Conn.AddCallback("471", func (e *irc.Event) {
		// Channel is full; try again later.
		log.Print("channel is full, retrying in 30 seconds...")
		time.Sleep(time.Second * 30)
		Conn.Join(Conf.Channel)
	})
	Conn.AddCallback("KICK", func (e *irc.Event) {
		if e.Arguments[1] == Conf.Nick {
			// Rejoin automatically after kick
			Conn.Join(Conf.Channel)
		}
	})
	Conn.AddCallback("PRIVMSG", handleCommand)
	err = Conn.Connect(Conf.Server)
	if err != nil {
		log.Printf("connection error: %s", err.Error())
		return;
	}
	defer Conn.Disconnect()

	err = OpenDatabase()
	if err != nil {
		log.Printf("database error: %s", err.Error())
		return;
	}

	Conn.Loop()
}
